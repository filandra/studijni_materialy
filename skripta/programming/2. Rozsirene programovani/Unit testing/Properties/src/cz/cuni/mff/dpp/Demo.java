/*******************************************************************************
 * Copyright 2016 Charles University in Prague
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *******************************************************************************/
package cz.cuni.mff.dpp;

import java.io.IOException;

/**
 * @author Dominik Skoda <skoda@d3s.mff.cuni.cz>
 *
 */
public class Demo {

	public static void main(String[] args) throws IOException {
		
		final String[] keys = new String[]{"A", "B", "C"};
		final String filename = "test.properties";
		
		Properties props = new Properties();
		props.set("A", "a");
		props.set("B", "b");
		props.save(filename);
		
		props.set("C", "c");
		props.set("A", "d");
		
		System.out.println("Before load:");
		printProperties(props, keys);
		
		props.load(filename);
		
		System.out.println("\nAfter load:");
		printProperties(props, keys);
		
	}
	
	private static void printProperties(Properties props, String[] keys){
		for(String key : keys){
			String value = props.get(key);
			if(value != null){
				System.out.println(String.format("%s = %s", key, value));
			}
		}
	}

}
