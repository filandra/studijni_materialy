/*******************************************************************************
 * Copyright 2016 Charles University in Prague
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *******************************************************************************/
package cz.cuni.mff.dpp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Stores <key, value> pairs.
 * 
 * @author Dominik Skoda <skoda@d3s.mff.cuni.cz>
 *
 */
public class Properties {
	
	final private Map<String, String> properties;
	
	public Properties(){
		properties = new HashMap<>();
	}
	
	public void set(String key, String value){
		properties.put(key, value);
	}
	
    /**
     *  Returns null for unknown key.
     */
    public String get(String key){
    	if(properties.containsKey(key)){
    		return properties.get(key);
    	}
    	return null;
    }
    
    /**
     * Save to file as
     * key = value
     * pairs on separate lines.
     */
    public void save(String filename) throws IOException {
    	FileWriter file = new FileWriter(filename);
    	for(String key : properties.keySet()){
    		file.write(String.format("%s = %s\n", key, properties.get(key)));
    	}
    	file.close();
    }
    
    /**
     *  Load from a file.
     */
    public void load(String filename) throws IOException {
    	properties.clear();
    	
    	BufferedReader file = new BufferedReader(new FileReader(filename));
    	final String delimiterExpr = " = ";
    	
		String line = file.readLine();
    	while(line != null){
    		String[] elements = line.split(delimiterExpr, 2);
    		String key = elements[0];
    		String value = elements[1];
    		properties.put(key, value);

    		line = file.readLine();
    	}
    	
    	file.close();
    }
}
