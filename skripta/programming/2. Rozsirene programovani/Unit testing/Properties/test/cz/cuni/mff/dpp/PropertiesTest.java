/*******************************************************************************
 * Copyright 2016 Charles University in Prague
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *******************************************************************************/
package cz.cuni.mff.dpp;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Dominik Skoda <skoda@d3s.mff.cuni.cz>
 *
 */
public class PropertiesTest {

	private Properties props;
	
	@Before
	public void setUp() throws Exception {
		props = new Properties();
	}

	@After
	public void tearDown() throws Exception {
		props = null;
	}

	@Test
	public void setGet_propertySet_propertyGot() {
		final String key = "Key";
		final String setValue = "value";
		props.set(key, setValue);
		final String gotValue = props.get(key);
		
		assertEquals(setValue, gotValue);
	}

	// TODO: write more tests to find bugs in the implementation
	
}
