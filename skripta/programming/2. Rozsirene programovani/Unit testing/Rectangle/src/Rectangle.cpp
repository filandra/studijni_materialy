
#include "Rectangle.h"
#include <cmath>
#include <vector>
#include <stdexcept>

using namespace test_rectangle;

const double EPSILON = 0.000001;

double distance(Point a, Point b){
	double dx = a.x - b.x;
	double dy = a.y - b.y;

	return std::sqrt(dx*dx + dy*dy);
}

double diagonalLength(double d1, double d2, double d3){
	return std::max(std::max(d1, d2), d3);
}

double shortSideLength(double d1, double d2, double d3){
	return std::min(std::min(d1, d2), d3);
}

double longSideLength(double d1, double d2, double d3){
	return std::max(std::min(d1,d2), std::min(std::max(d1,d2),d3));
}

bool impliesLengthEquals(double d1, double d2, double length){
	if(std::abs(d1 - length) < EPSILON &&
			std::abs(d2 - length) >= EPSILON){
		return false;
	}
	return true;
}

bool isRectangle(Point p1, Point p2, Point p3, Point p4){
	double d12 = distance(p1, p2);
	double d13 = distance(p1, p3);
	double d14 = distance(p1, p4);

	double d23 = distance(p2, p3);
	double d24 = distance(p2, p4);

	double d34 = distance(p3, p4);

	double diagonal = diagonalLength(d12, d13, d14);
	double shortSide = shortSideLength(d12, d13, 14);
	double longSide = longSideLength(d12, d13, d14);

	// Check diagonals are equal
	if(!impliesLengthEquals(d12, d34, diagonal)){
		return false;
	}
	if(!impliesLengthEquals(d13, d24, diagonal)){
		return false;
	}
	if(!impliesLengthEquals(d14, d23, diagonal)){
		return false;
	}

	// Check short sides
	if(!impliesLengthEquals(d12, d34, shortSide)){
		return false;
	}
	if(!impliesLengthEquals(d13, d24, shortSide)){
		return false;
	}
	if(!impliesLengthEquals(d14, d23, shortSide)){
		return false;
	}

	// Check short sides
	if(!impliesLengthEquals(d12, d34, longSide)){
		return false;
	}
	if(!impliesLengthEquals(d13, d24, longSide)){
		return false;
	}
	if(!impliesLengthEquals(d14, d23, longSide)){
		return false;
	}

	return true;
}

Rectangle::Rectangle( double l, double t, double r, double b ) :
		p1(l, b), p2(r, b), p3(r, t), p4(l, t){
	if(!isRectangle(p1, p2, p3, p4)){
		throw std::invalid_argument("Given points don't shape a rectangle.");
	}
}

Rectangle::Rectangle( Point p1, Point p2, Point p3, Point p4 ) :
		p1(p1), p2(p2), p3(p3), p4(p4){
	if(!isRectangle(p1, p2, p3, p4)){
		throw std::invalid_argument("Given points don't shape a rectangle.");
	}
}

double Rectangle::computeArea(){
	double d12 = distance(p1, p2);
	double d13 = distance(p1, p3);
	double d14 = distance(p1, p4);

	double shortSide = shortSideLength(d12, d13, 14);
	double longSide = longSideLength(d12, d13, d14);

	return shortSide * longSide;
}

void Rectangle::rotate( double radians ){
	// calculate center
	double minX = std::min(std::min(p1.x, p2.x), std::min(p3.x, p4.x));
	double maxX = std::max(std::max(p1.x, p2.x), std::max(p3.x, p4.x));
	double minY = std::min(std::min(p1.y, p2.y), std::min(p3.y, p4.y));
	double maxY = std::max(std::max(p1.y, p2.y), std::max(p3.y, p4.y));
	Point center((maxX + minX)/2, (maxY + minY)/2);

	// move the rectangle so the center is in [0, 0]
	move(center.x * -1, center.y * -1);

	// rotate
	Point p1r(p1.x * std::cos(radians) - p1.y * std::sin(radians),
			p1.y * std::cos(radians) + p1.x * std::sin(radians));
	p1 = p1r;
	Point p2r(p2.x * std::cos(radians) - p2.y * std::sin(radians),
			p2.y * std::cos(radians) + p2.x * std::sin(radians));
	p2 = p2r;
	Point p3r(p3.x * std::cos(radians) - p3.y * std::sin(radians),
			p3.y * std::cos(radians) + p3.x * std::sin(radians));
	p3 = p3r;
	Point p4r(p4.x * std::cos(radians) - p4.y * std::sin(radians),
			p4.y * std::cos(radians) + p4.x * std::sin(radians));
	p4 = p4r;

	// move back
	move(center.x, center.y);
}

void Rectangle::move( double x, double y ){
	p1.x += x;
	p1.y += y;

	p2.x += x;
	p2.y += y;

	p3.x += x;
	p3.y += y;

	p4.x += x;
	p4.y += y;
}

std::vector<Point> Rectangle::getPoints(){
	std::vector<Point> res;
	res.push_back(p1);
	res.push_back(p2);
	res.push_back(p3);
	res.push_back(p4);

	return res;
}

