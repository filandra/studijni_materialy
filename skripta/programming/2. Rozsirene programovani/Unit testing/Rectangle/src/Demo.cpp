//============================================================================
// Name        : TestRectangle.cpp
// Author      : Dominik Skoda
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <stdexcept>
#include "Rectangle.h"

using namespace test_rectangle;

void printRect(Rectangle r){
	std::vector<Point> points = r.getPoints();
	for(std::vector<Point>::iterator i = points.begin(); i != points.end(); i++){
		std::cout << " [" << i->x << ", " << i->y << "] ";
	}
	std::cout << std::endl;
}

int main() {

	try {
		Rectangle r(Point(-1, -1), Point(1, -1), Point(1, 1), Point(-1, 1));

		std::cout << "\nRectangle:" << std::endl;
		printRect(r);
		std::cout << "Area: " << r.computeArea() << std::endl;

		r.move(5, 2);

		std::cout << "\nmove [5, 2]:" << std::endl;
		printRect(r);
		std::cout << "Area: " << r.computeArea() << std::endl;

		double pi_4	= 0.78539816339; // pi/4 = 45�
		r.rotate(pi_4 /* Radians */ );

		std::cout << "\nrotate " << pi_4 << ":" << std::endl;
		printRect(r);
		std::cout << "Area: " << r.computeArea() << std::endl;

		double pi_7_4 = 5.49778714378; // 2pi - pi/4 = 315�
		r.rotate(pi_7_4  /* Radians */ );

		std::cout << "\nrotate " << pi_7_4 << ":" << std::endl;
		printRect(r);
		std::cout << "Area: " << r.computeArea() << std::endl;
	} catch(const std::exception &e){
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
