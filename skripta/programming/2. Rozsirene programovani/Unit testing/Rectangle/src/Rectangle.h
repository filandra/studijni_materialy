/*
 * Rectangle.h
 *
 *  Created on: 20. 4. 2016
 *      Author: iridi
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include <vector>

namespace test_rectangle {

class Point {
public:
	double x, y;

	Point(double x, double y){
		this->x = x;
		this->y = y;
	}
};

class Rectangle {
    public:
    Rectangle( double l, double t, double r, double b );
    Rectangle( Point p1, Point p2, Point p3, Point p4 );

    double computeArea();

    void rotate( double radians );
    void move( double x, double y );

    std::vector<Point> getPoints();

    private:
    Point p1, p2, p3, p4; // lower/upper left/right
};

}

#endif /* RECTANGLE_H_ */
