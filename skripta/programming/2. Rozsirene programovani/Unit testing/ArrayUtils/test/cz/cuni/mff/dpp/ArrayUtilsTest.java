/*******************************************************************************
 * Copyright 2016 Charles University in Prague
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *******************************************************************************/
package cz.cuni.mff.dpp;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Dominik Skoda <skoda@d3s.mff.cuni.cz>
 *
 */
public class ArrayUtilsTest {

	private ArrayUtils joiner;
	
	@Before
	public void setUp() throws Exception {
		joiner = new ArrayUtils();
	}

	@After
	public void tearDown() throws Exception {
		joiner = null;
	}

	@Test
	public void join_ArgsOk_result() {
		final String expected = "'a', 'b', 'c'";
		
		List<String> items = Arrays.asList(new String[]{"a", "b", "c"});
		String result = joiner.join(items, ", ", "'", "'");
		
		assertEquals(expected, result);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void join_arg1null_exception() {
		joiner.join(null, ", ", "'", "'");
	}
	
	// TODO: make more tests, try to find all bugs

}
