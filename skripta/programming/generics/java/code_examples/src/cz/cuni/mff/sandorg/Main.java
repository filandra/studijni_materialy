package cz.cuni.mff.sandorg;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public interface List<E> {
        void add(E x);

        Iterator<E> iterator();

        E get(int i);
    }

    public interface Iterator<E> {
        E next();

        boolean hasNext();
    }

    public interface IntegerList {
        void add(Integer x);

        Iterator<IntegerList> iterator();
    }

    ArrayList<Integer> list = new ArrayList<Integer>();
    ArrayList<ArrayList<Integer>> list2 = new ArrayList<ArrayList<Integer>>();

    ArrayList<Integer> list = new ArrayList<>();
    ArrayList<ArrayList<Integer>> list2 = new ArrayList<>();

import java.util.*;

class Geeks {
    //Here, T will be replaced by String i.e. java.lang.String
    String str;

    Geeks(String o) {
        str = o;
    }

    String getob() {
        return str;
    }
}


}
