package cz.sandor.statnice.variance;

public class VirtualMethods {

//    class A {
//        public void print() {
//            System.out.println("Hello from A");
//        }
//    }
//
//    class B extends A {
//        public void print() {
//            System.out.println("Hello from B");
//        }
//    }

    public void testVirtuals() {
        A a = new A();
        B b = new B();
        A ab = new B();

        a.print();
        b.print();
        ab.print();
    }
}
