package cz.sandor.statnice.variance;

public class OverrideOverload {
    class Animal {
        public void beep(String name) {
            System.out.println("beeeep " + name);
        }
    }

    class Dog extends Animal {
        @Override
        public void beep(String name) { // overrides, parameter type remains the same
            System.out.println("waf waf " + name);
        }

        public void beep(int count) { // overloads, parameter changes
            for (int i = 0; i < count; i++) {
                System.out.println("waauuuuu");
            }
        }
    }
}
