package cz.sandor.statnice.variance;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        new VirtualMethods().testVirtuals();
        //Main main = new Main();
        //main.explain();
    }

    class Vehicle {}
    class Car extends Vehicle {
    }

    class MyClass<T> {
    }

    public void explain() {
//        Integer[] integers = new Integer[10];
//        Object[] objs = integers; // Now we have a variable objectArray that is seen by the compiler as an array of
//        // objects, but underneath what we really have is an array of integers.
//
//        objs[0] = new Car(); // Exception in thread "main" java.lang.ArrayStoreException: cz.sandor.statnice.variance.Main$Car
//        objs[1] = "string";
//
//        // But, our array of integers is now an array of objects in the eyes of the compiler! We are allowed to store
//        // strings and triangles and bananas — the compiler will just stand there and allow it to happen. Of course,
//        // at runtime our code will crash and burn when we get stuff from the original variable holding an array of
//        // integers (or, more precisely, variable which *thinks* it’s still holding an array of integers while in
//        // reality it’s holding all kinds of stuff).
//
//        // Generics allow any class to have a type tag just like an array does, but unlike arrays, these type tags
//        // are not covariant.
//
//        MyClass<String> stringClass = new MyClass<Object>(); // MyClass<String> is not a subclass of MyClass<Object>
//
//        MyClass<String> stringMyClass = new MyClass<String>();
//        acceptOnlyObject(stringMyClass); // Method that takes MyClass<Object> cannot be passed an instance of MyClass<String>.
    }

    public void acceptOnlyObject(MyClass<Object> objectOnly) {    }

    // Upper bound allows one to restrict the type “from above”, that is, to specify what’s the highest level of
    // hierarchy allowed. This means that process() accepts List parameterized by Car or any subclass of Car.
    // Yay, covariance!!!!
    public void process1(List<? extends Car> cars) {}

    // Lower bound restricts the type “from below” by specifying what’s the lowest type that’s allowed.
    // Now process() only accepts Lists parameterized by Car or any superclass of Car, thus achieving contravariance.
    // e.g Car is a subclass of Vehicle, but the list will accepts only the superclasses of Car.
    // In other words: Car is an extension of the Vehicle, but the method will accepts only LESS extended classes.
    // Car <: Vehicle ==> f(Car) :> f(Vehicle)
    public void process2(List<? super Car> fruits) {}
}
