\contentsline {chapter}{\numberline {1}Slo\IeC {\v z}itost a vy\IeC {\v c}\IeC {\'\i }slitelnost}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}V\IeC {\'y}po\IeC {\v c}etn\IeC {\'\i } modely (Turingovy stroje, RAM)}{7}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Turingovy stroje}{7}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}RAM}{8}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}D\IeC {\r u}kaz - RAM na TS a TS na RAM}{9}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Godelovo \IeC {\v c}\IeC {\'\i }slo}{9}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Univerz\IeC {\'a}ln\IeC {\'\i } TS}{9}{subsection.1.1.5}
\contentsline {section}{\numberline {1.2}Algoritmicky nerozhodnuteln\IeC {\'e} probl\IeC {\'e}my (halting problem)}{10}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Postova v\IeC {\v e}ta}{10}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}D\IeC {\r u}kaz - Diagonaliza\IeC {\v c}n\IeC {\'\i } jazyk nen\IeC {\'\i } \IeC {\v c}\IeC {\'a}ste\IeC {\v c}n\IeC {\v e} rozhodnuteln\IeC {\'y}}{10}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}D\IeC {\r u}kaz - \IeC {\v C}\IeC {\'a}st\IeC {\v e}\IeC {\v c}n\IeC {\'a} rozhodnutelnost univerz\IeC {\'a}ln\IeC {\'\i }ho jazyka}{11}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}D\IeC {\r u}kaz - \IeC {\v C}\IeC {\'a}st\IeC {\v e}\IeC {\v c}n\IeC {\'a} rozhodnutelnost halting probl\IeC {\'e}mu}{11}{subsection.1.2.4}
\contentsline {section}{\numberline {1.3}Rozhodnuteln\IeC {\'e} a \IeC {\v c}\IeC {\'a}ste\IeC {\v c}n\IeC {\v e} rozhodnuteln\IeC {\'e} probl\IeC {\'e}my}{11}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}D\IeC {\r u}kaz - Riceova v\IeC {\v e}ta}{13}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}Nedeterministick\IeC {\'y} v\IeC {\'y}po\IeC {\v c}etn\IeC {\'\i } model}{13}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}D\IeC {\r u}kaz - Pro ka\IeC {\v z}d\IeC {\'y} NTS existuje DTS, kter\IeC {\'y} simuluje jeho pr\IeC {\'a}ci}{13}{subsection.1.4.1}
\contentsline {section}{\numberline {1.5}Z\IeC {\'a}kladn\IeC {\'\i } t\IeC {\v r}\IeC {\'\i }dy slo\IeC {\v z}itosti a jejich vztahy}{13}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}V\IeC {\v e}ta NP = union nedeterministick\IeC {\'y} polynomi\IeC {\'a}ln\IeC {\'\i }}{14}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}D\IeC {\r u}kaz - Vztahy mezi t\IeC {\v r}\IeC {\'\i }dami}{15}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}D\IeC {\r u}kaz - NPSPACE je podmno\IeC {\v z}ina EXPTIME}{15}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}V\IeC {\v s}echny vztahy}{15}{subsection.1.5.4}
\contentsline {subsection}{\numberline {1.5.5}Savi\IeC {\v c}ova v\IeC {\v e}ta - jenom zn\IeC {\v e}n\IeC {\'\i } a d\IeC {\r u}sledek}{15}{subsection.1.5.5}
\contentsline {section}{\numberline {1.6}V\IeC {\v e}ty o hierarchii}{16}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}D\IeC {\r u}kaz - v\IeC {\v e}ta o deterministick\IeC {\'e} prostorov\IeC {\'e} hiearchii a d\IeC {\r u}sledky}{16}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}D\IeC {\r u}kaz - v\IeC {\v e}ta o deterministick\IeC {\'e} \IeC {\v c}asov\IeC {\'e} hiearchii a d\IeC {\r u}sledky}{17}{subsection.1.6.2}
\contentsline {section}{\numberline {1.7}\IeC {\'U}pln\IeC {\'e} probl\IeC {\'e}my pro t\IeC {\v r}\IeC {\'\i }du NP, Cook-Levinova v\IeC {\v e}ta}{17}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}D\IeC {\r u}kaz - Cook-levinova v\IeC {\v e}ta}{18}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}D\IeC {\r u}kaz - SAT je NP-\IeC {\'u}pln\IeC {\'y}}{19}{subsection.1.7.2}
\contentsline {subsection}{\numberline {1.7.3}D\IeC {\r u}kaz - 3SAT je NP-\IeC {\'u}pln\IeC {\'y}}{19}{subsection.1.7.3}
\contentsline {subsection}{\numberline {1.7.4}Dal\IeC {\v s}\IeC {\'\i } NP-\IeC {\'u}pln\IeC {\'e} probl\IeC {\'e}my}{19}{subsection.1.7.4}
\contentsline {section}{\numberline {1.8}Pseudopolynomi\IeC {\'a}ln\IeC {\'\i } algoritmy, siln\IeC {\'a} NP-\IeC {\'u}plnost}{19}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Pseudopolynomi\IeC {\'a}lnost}{19}{subsection.1.8.1}
\contentsline {subsection}{\numberline {1.8.2}Siln\IeC {\'a} NP-\IeC {\'u}plnost}{20}{subsection.1.8.2}
\contentsline {subsection}{\numberline {1.8.3}D\IeC {\r u}kaz - obchodn\IeC {\'\i } cestuj\IeC {\'\i }c\IeC {\'\i } je siln\IeC {\v e} NP-\IeC {\'u}pln\IeC {\'y}}{21}{subsection.1.8.3}
\contentsline {section}{\numberline {1.9}Aproxima\IeC {\v c}n\IeC {\'\i } algoritmy a sch\IeC {\'e}mata}{21}{section.1.9}
\contentsline {subsection}{\numberline {1.9.1}D\IeC {\r u}kaz - FF Bin Packing}{22}{subsection.1.9.1}
\contentsline {subsection}{\numberline {1.9.2}Aproxima\IeC {\v c}n\IeC {\'\i } sch\IeC {\'e}mata}{22}{subsection.1.9.2}
\contentsline {subsection}{\numberline {1.9.3}D\IeC {\r u}kaz - Aproxima\IeC {\v c}n\IeC {\'\i } sch\IeC {\'e}mata a NP-\IeC {\'u}plnost}{23}{subsection.1.9.3}
\contentsline {subsection}{\numberline {1.9.4}D\IeC {\r u}kaz - Neaproximovatelnost}{23}{subsection.1.9.4}
\contentsline {chapter}{\numberline {2}Datov\IeC {\'e} struktury}{25}{chapter.2}
\contentsline {section}{\numberline {2.1}Vyhled\IeC {\'a}vac\IeC {\'\i } stromy ((a,b)-stromy, Splay stromy)}{25}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}BB[1/2]-stromy}{25}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Splay stromy}{25}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}(a,b)-stromy}{26}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}\IeC {\v C}erveno-\IeC {\v c}ern\IeC {\'e} stromy}{27}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Haldy (regul\IeC {\'a}rn\IeC {\'\i }, binomi\IeC {\'a}ln\IeC {\'\i })}{27}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}D-regul\IeC {\'a}rn\IeC {\'\i } halda}{28}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Binomi\IeC {\'a}ln\IeC {\'\i } halda}{28}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}L\IeC {\'\i }n\IeC {\'a} Binomi\IeC {\'a}ln\IeC {\'\i } halda}{29}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Fibonnaciho halda}{29}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Ha\IeC {\v s}ov\IeC {\'a}n\IeC {\'\i }, \IeC {\v r}e\IeC {\v s}en\IeC {\'\i } koliz\IeC {\'\i }, univerz\IeC {\'a}ln\IeC {\'\i } ha\IeC {\v s}ov\IeC {\'a}n\IeC {\'\i }, v\IeC {\'y}b\IeC {\v e}r ha\IeC {\v s}ovac\IeC {\'\i } funkce}{29}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Line\IeC {\'a}rn\IeC {\'\i } p\IeC {\v r}id\IeC {\'a}v\IeC {\'a}n\IeC {\'\i }}{30}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Perfektn\IeC {\'\i } hashov\IeC {\'a}n\IeC {\'\i }}{30}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Kuka\IeC {\v c}\IeC {\v c}\IeC {\'\i } hashov\IeC {\'a}n\IeC {\'\i }}{31}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Anal\IeC {\'y}za nejhor\IeC {\v s}\IeC {\'\i }ho, amortizovan\IeC {\'e}ho a o\IeC {\v c}ek\IeC {\'a}van\IeC {\'e}ho chov\IeC {\'a}n\IeC {\'\i } datov\IeC {\'y}ch struktur}{32}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Nafukovac\IeC {\'\i } pole - agrega\IeC {\v c}n\IeC {\'\i } metoda}{33}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Zmen\IeC {\v s}ovac\IeC {\'\i } pole - \IeC {\'u}\IeC {\v c}etn\IeC {\'\i } metoda}{33}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Bin\IeC {\'a}rn\IeC {\'\i } pole - pen\IeC {\'\i }zkov\IeC {\'a} metoda}{33}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Potenci\IeC {\'a}lov\IeC {\'a} metoda}{34}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Chov\IeC {\'a}n\IeC {\'\i } a anal\IeC {\'y}za datov\IeC {\'y}ch struktur na syst\IeC {\'e}mech s pam\IeC {\v e}\IeC {\v t}ovou hierarchi\IeC {\'\i }}{34}{section.2.5}
\contentsline {chapter}{\numberline {3}Anal\IeC {\'y}za a architektury software}{39}{chapter.3}
\contentsline {section}{\numberline {3.1}Procesy v\IeC {\'y}voje SW a jejich f\IeC {\'a}ze}{39}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}F\IeC {\'a}ze}{39}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Metody v\IeC {\'y}voje}{40}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Reuirements Engineering}{42}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Evoluce softwaru}{44}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Podnikov\IeC {\'e} procesy a jejich modelov\IeC {\'a}n\IeC {\'\i } pomoc\IeC {\'\i } BPMN}{44}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Business Driven Development - BDD}{45}{subsection.3.2.1}
\contentsline {subsubsection}{LF - Modeling}{46}{section*.2}
\contentsline {subsubsection}{LF - Development}{46}{section*.3}
\contentsline {subsubsection}{LF - Deployment}{47}{section*.4}
\contentsline {subsubsection}{LF - Monitoring}{47}{section*.5}
\contentsline {subsubsection}{LF - Adaptation}{47}{section*.6}
\contentsline {subsection}{\numberline {3.2.2}BPMN}{47}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}UML a jeho vyu\IeC {\v z}it\IeC {\'\i } pro anal\IeC {\'y}zu a n\IeC {\'a}vrh struktury a chov\IeC {\'a}n\IeC {\'\i } SW}{48}{section.3.3}
\contentsline {section}{\numberline {3.4}N\IeC {\'a}vrhov\IeC {\'e} vzory}{49}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Layer}{49}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Broker}{50}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}MVC}{50}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Pipe and Filter}{51}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Client-Server}{51}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Peer-to-peer}{51}{subsection.3.4.6}
\contentsline {subsection}{\numberline {3.4.7}Publish subscribe}{52}{subsection.3.4.7}
\contentsline {subsection}{\numberline {3.4.8}Map-Reduce}{52}{subsection.3.4.8}
\contentsline {section}{\numberline {3.5}Testov\IeC {\'a}n\IeC {\'\i } SW, dopadov\IeC {\'a} a zm\IeC {\v e}nov\IeC {\'a} anal\IeC {\'y}za}{53}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Unit testing}{54}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Component/Integra\IeC {\v c}n\IeC {\'\i } testing}{55}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}System testing}{55}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Test driven development}{55}{subsection.3.5.4}
\contentsline {section}{\numberline {3.6}Pl\IeC {\'a}nov\IeC {\'a}n\IeC {\'\i } SW projekt\IeC {\r u}, odhad n\IeC {\'a}klad\IeC {\r u}, \IeC {\'u}rovn\IeC {\v e} \IeC {\v r}\IeC {\'\i }zen\IeC {\'\i } projekt\IeC {\r u}}{55}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Risk management}{56}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Pl\IeC {\'a}nov\IeC {\'a}n\IeC {\'\i } projektu}{56}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Project scheduling}{57}{subsection.3.6.3}
\contentsline {section}{\numberline {3.7}Pr\IeC {\'a}vn\IeC {\'\i } aspekty SW, hlavn\IeC {\'\i } z\IeC {\'a}kony d\IeC {\r u}le\IeC {\v z}it\IeC {\'e} pro IT projekty}{57}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Autorsk\IeC {\'e} pr\IeC {\'a}vo}{57}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Patentov\IeC {\'a} ochrana software}{58}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Pr\IeC {\'a}vn\IeC {\'\i } ochrana datab\IeC {\'a}z\IeC {\'\i }}{59}{subsection.3.7.3}
\contentsline {section}{\numberline {3.8}Typy pohled\IeC {\r u} na SW architekturu}{59}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Bass, Clemens, Kazman pohledy}{60}{subsection.3.8.1}
\contentsline {subsubsection}{Module viewpoint}{60}{section*.7}
\contentsline {subsubsection}{Component-and-connector viewpoint}{61}{section*.8}
\contentsline {subsubsection}{Allocation viewpoint}{62}{section*.9}
\contentsline {subsection}{\numberline {3.8.2}Kruchten's 4+1 View}{62}{subsection.3.8.2}
\contentsline {subsubsection}{Logical view}{62}{section*.10}
\contentsline {subsubsection}{Development view}{62}{section*.11}
\contentsline {subsubsection}{Process view}{63}{section*.12}
\contentsline {subsubsection}{Physical view}{63}{section*.13}
\contentsline {subsubsection}{Scenarios}{63}{section*.14}
\contentsline {section}{\numberline {3.9}Modelov\IeC {\'a}n\IeC {\'\i } a dokumentace SW architektury - TODO}{63}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Form\IeC {\'a}ln\IeC {\'\i } modelov\IeC {\'a}n\IeC {\'\i } - TODO}{63}{subsection.3.9.1}
\contentsline {subsection}{\numberline {3.9.2}Organiza\IeC {\v c}n\IeC {\'\i } architektura}{65}{subsection.3.9.2}
\contentsline {subsection}{\numberline {3.9.3}Informa\IeC {\v c}n\IeC {\'\i } architektura}{65}{subsection.3.9.3}
\contentsline {subsection}{\numberline {3.9.4}Modelem \IeC {\v r}\IeC {\'\i }zen\IeC {\'y} v\IeC {\'y}voj}{66}{subsection.3.9.4}
\contentsline {section}{\numberline {3.10}Klasifikace atribut\IeC {\r u} kvality SW architektury, jejich popis pomoc\IeC {\'\i } sc\IeC {\'e}n\IeC {\'a}\IeC {\v r}\IeC {\r u} a taktik}{66}{section.3.10}
\contentsline {subsection}{\numberline {3.10.1}Availability}{66}{subsection.3.10.1}
\contentsline {subsection}{\numberline {3.10.2}Modifiability}{67}{subsection.3.10.2}
\contentsline {subsection}{\numberline {3.10.3}Performance}{68}{subsection.3.10.3}
\contentsline {subsection}{\numberline {3.10.4}Security}{68}{subsection.3.10.4}
\contentsline {subsection}{\numberline {3.10.5}Interoperability}{69}{subsection.3.10.5}
\contentsline {subsection}{\numberline {3.10.6}Testability}{69}{subsection.3.10.6}
\contentsline {subsection}{\numberline {3.10.7}Usability}{70}{subsection.3.10.7}
\contentsline {section}{\numberline {3.11}Servisn\IeC {\v e} orientovan\IeC {\'e} architektury}{71}{section.3.11}
\contentsline {subsubsection}{Standardizace}{71}{section*.15}
\contentsline {subsubsection}{Loose-coupling}{71}{section*.16}
\contentsline {subsubsection}{Abstrakce}{72}{section*.17}
\contentsline {subsubsection}{Znovupou\IeC {\v z}itelnost}{72}{section*.18}
\contentsline {subsubsection}{Autonomie}{72}{section*.19}
\contentsline {subsubsection}{Bezestavovost}{72}{section*.20}
\contentsline {subsubsection}{Vyhledatelnost}{72}{section*.21}
\contentsline {subsubsection}{Composability}{73}{section*.22}
\contentsline {section}{\numberline {3.12}Algebraick\IeC {\'e} metody, v\IeC {\'\i }cedruhov\IeC {\'e} algebry, inici\IeC {\'a}ln\IeC {\'\i } modely - TODO}{73}{section.3.12}
\contentsline {section}{\numberline {3.13}Form\IeC {\'a}ln\IeC {\'\i } z\IeC {\'a}klady jazyka UML}{73}{section.3.13}
\contentsline {subsection}{\numberline {3.13.1}UML class diagram}{73}{subsection.3.13.1}
\contentsline {section}{\numberline {3.14}OCL jako specifika\IeC {\v c}n\IeC {\'\i } jazyk a form\IeC {\'a}ln\IeC {\'\i } z\IeC {\'a}klady dle specifikace}{74}{section.3.14}
\contentsline {section}{\numberline {3.15}Form\IeC {\'a}ln\IeC {\'\i } z\IeC {\'a}klady RDF a jazyka OWL, deskrip\IeC {\v c}n\IeC {\'\i } logika}{75}{section.3.15}
\contentsline {subsection}{\numberline {3.15.1}Deskrip\IeC {\v c}n\IeC {\'\i } logika}{75}{subsection.3.15.1}
\contentsline {subsection}{\numberline {3.15.2}OWL}{77}{subsection.3.15.2}
\contentsline {chapter}{\numberline {4}Roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'e} programov\IeC {\'a}n\IeC {\'\i }}{79}{chapter.4}
\contentsline {section}{\numberline {4.1}Obecn\IeC {\v e}}{79}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}C\# }{80}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Java}{82}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}C++}{84}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Objektov\IeC {\'e} koncepty modern\IeC {\'\i }ch jazyk\IeC {\r u}}{87}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Variance}{88}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}D\IeC {\v e}di\IeC {\v c}nost}{89}{subsection.4.2.2}
\contentsline {subsubsection}{C\# }{90}{section*.23}
\contentsline {subsubsection}{Java}{90}{section*.24}
\contentsline {subsubsection}{C++}{90}{section*.25}
\contentsline {subsection}{\numberline {4.2.3}Virtu\IeC {\'a}ln\IeC {\'\i } metody}{91}{subsection.4.2.3}
\contentsline {subsubsection}{C\# }{92}{section*.26}
\contentsline {subsubsection}{Java}{92}{section*.27}
\contentsline {subsubsection}{C++}{92}{section*.28}
\contentsline {section}{\numberline {4.3}Generick\IeC {\'e} programov\IeC {\'a}n\IeC {\'\i } a metaprogramov\IeC {\'a}n\IeC {\'\i }, generika a \IeC {\v s}ablony, politiky, traits, type inference}{93}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Generick\IeC {\'e} programov\IeC {\'a}n\IeC {\'\i }}{93}{subsection.4.3.1}
\contentsline {subsubsection}{C\#}{93}{section*.29}
\contentsline {subsubsection}{Java}{94}{section*.30}
\contentsline {subsubsection}{C++}{95}{section*.31}
\contentsline {subsection}{\numberline {4.3.2}Metaprogramov\IeC {\'a}n\IeC {\'\i } - TODO GROOVY}{96}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}C\# }{97}{subsection.4.3.3}
\contentsline {subsubsection}{Java}{97}{section*.32}
\contentsline {subsubsection}{C++}{98}{section*.33}
\contentsline {subsection}{\numberline {4.3.4}Politiky a traits}{98}{subsection.4.3.4}
\contentsline {subsubsection}{C++}{98}{section*.34}
\contentsline {subsection}{\numberline {4.3.5}Type inference}{99}{subsection.4.3.5}
\contentsline {subsubsection}{C\#}{99}{section*.35}
\contentsline {subsubsection}{Java}{100}{section*.36}
\contentsline {subsubsection}{C++}{100}{section*.37}
\contentsline {section}{\numberline {4.4}Modern\IeC {\'\i } konstrukce programovac\IeC {\'\i }ch jazyk\IeC {\r u} - TODO functor a mon\IeC {\'a}dy}{100}{section.4.4}
\contentsline {section}{\numberline {4.5}Odkazy na objekty a jejich \IeC {\v z}ivotnost}{101}{section.4.5}
\contentsline {section}{\numberline {4.6}Pokro\IeC {\v c}il\IeC {\'e} aspekty imperativn\IeC {\'\i }ch jazyk\IeC {\r u}}{101}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Real-time syst\IeC {\'e}my}{102}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Java Anotace}{102}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}RMI}{103}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Java ClassLoader}{103}{subsection.4.6.4}
\contentsline {subsection}{\numberline {4.6.5}Java native interface a access}{103}{subsection.4.6.5}
\contentsline {subsection}{\numberline {4.6.6}Aspektov\IeC {\v e} orientovan\IeC {\'e} programov\IeC {\'a}n\IeC {\'\i }}{104}{subsection.4.6.6}
\contentsline {subsection}{\numberline {4.6.7}JavaBeans}{105}{subsection.4.6.7}
\contentsline {subsection}{\numberline {4.6.8}Servlety}{105}{subsection.4.6.8}
\contentsline {subsection}{\numberline {4.6.9}C\# Deleg\IeC {\'a}ti}{106}{subsection.4.6.9}
\contentsline {subsection}{\numberline {4.6.10}Konkurence}{107}{subsection.4.6.10}
\contentsline {subsubsection}{Java}{107}{section*.38}
\contentsline {subsubsection}{C++}{108}{section*.39}
\contentsline {subsubsection}{C\# - TODO async eventu\IeC {\'a}ln\IeC {\v e}}{110}{section*.40}
\contentsline {section}{\numberline {4.7}V\IeC {\'y}jimky a bezpe\IeC {\v c}n\IeC {\'e} programov\IeC {\'a}n\IeC {\'\i } v prost\IeC {\v r}ed\IeC {\'\i } s v\IeC {\'y}jimkami}{110}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}C\# }{110}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Java}{111}{subsection.4.7.2}
\contentsline {subsection}{\numberline {4.7.3}C++}{111}{subsection.4.7.3}
\contentsline {section}{\numberline {4.8}Implementace objektov\IeC {\'y}ch vlastnost\IeC {\'\i }, b\IeC {\v e}hov\IeC {\'a} podpora, volac\IeC {\'\i } konvence, garbage collection}{112}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}GC}{114}{subsection.4.8.1}
\contentsline {subsubsection}{C\# }{114}{section*.41}
\contentsline {subsubsection}{Java}{115}{section*.42}
\contentsline {subsubsection}{C++}{115}{section*.43}
\contentsline {section}{\numberline {4.9}Vliv modern\IeC {\'\i }ch konstrukc\IeC {\'\i } na v\IeC {\'y}konnost k\IeC {\'o}du - TODO}{115}{section.4.9}
\contentsline {section}{\numberline {4.10}N\IeC {\'a}vrhov\IeC {\'e} vzory a jejich vyu\IeC {\v z}it\IeC {\'\i }}{115}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}Creational - Factory method}{115}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}Creational - Abstract factory}{115}{subsection.4.10.2}
\contentsline {subsection}{\numberline {4.10.3}Creational - Builder}{115}{subsection.4.10.3}
\contentsline {subsection}{\numberline {4.10.4}Creational - Prototype}{116}{subsection.4.10.4}
\contentsline {subsection}{\numberline {4.10.5}Creational - Singleton}{116}{subsection.4.10.5}
\contentsline {subsection}{\numberline {4.10.6}Structural - Adapter}{116}{subsection.4.10.6}
\contentsline {subsection}{\numberline {4.10.7}Structural - Bridge}{117}{subsection.4.10.7}
\contentsline {subsection}{\numberline {4.10.8}Structural - Composite}{117}{subsection.4.10.8}
\contentsline {subsection}{\numberline {4.10.9}Structural - Decorator}{117}{subsection.4.10.9}
\contentsline {subsection}{\numberline {4.10.10}Structural - Facade}{118}{subsection.4.10.10}
\contentsline {subsection}{\numberline {4.10.11}Structural - Flyweight}{118}{subsection.4.10.11}
\contentsline {subsection}{\numberline {4.10.12}Structural - Proxy}{118}{subsection.4.10.12}
\contentsline {subsection}{\numberline {4.10.13}Behavioral - Chain of responsibility}{119}{subsection.4.10.13}
\contentsline {subsection}{\numberline {4.10.14}Behavioral - Command}{119}{subsection.4.10.14}
\contentsline {subsection}{\numberline {4.10.15}Behavioral - Interpreter}{119}{subsection.4.10.15}
\contentsline {subsection}{\numberline {4.10.16}Behavioral - Iterator}{120}{subsection.4.10.16}
\contentsline {subsection}{\numberline {4.10.17}Behavioral - Mediator}{120}{subsection.4.10.17}
\contentsline {subsection}{\numberline {4.10.18}Behavioral - Memento}{120}{subsection.4.10.18}
\contentsline {subsection}{\numberline {4.10.19}Behavioral - Observer}{120}{subsection.4.10.19}
\contentsline {subsection}{\numberline {4.10.20}Behavioral - State}{121}{subsection.4.10.20}
\contentsline {subsection}{\numberline {4.10.21}Behavioral - Strategy}{121}{subsection.4.10.21}
\contentsline {subsection}{\numberline {4.10.22}Behavioral - Template method}{121}{subsection.4.10.22}
\contentsline {subsection}{\numberline {4.10.23}Behavioral - Visitor}{122}{subsection.4.10.23}
\contentsline {section}{\numberline {4.11}Skriptovac\IeC {\'\i } jazyky, prototype-based jazyky, koncepty jazyk\IeC {\r u} bez t\IeC {\v r}\IeC {\'\i }d}{122}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}Java Scripting API}{122}{subsection.4.11.1}
\contentsline {section}{\numberline {4.12}Domain Specific Languages}{123}{section.4.12}
\contentsline {section}{\numberline {4.13}Funkcion\IeC {\'a}ln\IeC {\'\i } programov\IeC {\'a}n\IeC {\'\i }}{123}{section.4.13}
\contentsline {section}{\numberline {4.14}Principy tvorby kvalitn\IeC {\'\i }ho k\IeC {\'o}du, doporu\IeC {\v c}en\IeC {\'e} postupy, refaktorizace}{124}{section.4.14}
\contentsline {section}{\numberline {4.15}Testov\IeC {\'a}n\IeC {\'\i } funk\IeC {\v c}nosti, hled\IeC {\'a}n\IeC {\'\i } chyb, monitorov\IeC {\'a}n\IeC {\'\i } program\IeC {\r u}}{127}{section.4.15}
\contentsline {chapter}{\numberline {5}Webov\IeC {\'e} technologie}{131}{chapter.5}
\contentsline {section}{\numberline {5.1}Obecn\IeC {\'y} p\IeC {\v r}ehled z\IeC {\'a}kladn\IeC {\'\i } webov\IeC {\'y}ch technologi\IeC {\'\i }}{131}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Taxonomie po\IeC {\v c}\IeC {\'\i }ta\IeC {\v c}ov\IeC {\'y}ch s\IeC {\'\i }t\IeC {\'\i }. Architektura ISO/OSI. P\IeC {\v r}ehled s\IeC {\'\i }\IeC {\v t}ov\IeC {\'e}ho modelu TCP/IP}{132}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Sm\IeC {\v e}rov\IeC {\'a}n\IeC {\'\i }, koncept adresy, portu, socketu}{133}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Principy datov\IeC {\'y}ch p\IeC {\v r}enos\IeC {\r u} - k\IeC {\'o}dov\IeC {\'a}n\IeC {\'\i }, modulace, p\IeC {\v r}enosov\IeC {\'a} rychlost a p\IeC {\'a}smo, Nyquist\IeC {\r u}v a Shannon\IeC {\r u}v teor\IeC {\'e}m, analogov\IeC {\'y}/digit\IeC {\'a}ln\IeC {\'\i } p\IeC {\v r}enos, p\IeC {\v r}enosov\IeC {\'a} m\IeC {\'e}dia}{135}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Techniky p\IeC {\v r}enosu dat - synchronn\IeC {\'\i }/asynchronn\IeC {\'\i } p\IeC {\v r}enosy, CRC, potvrzov\IeC {\'a}n\IeC {\'\i }, \IeC {\v r}\IeC {\'\i }zen\IeC {\'\i } toku}{138}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}P\IeC {\v r}\IeC {\'\i }stupov\IeC {\'e} metody, \IeC {\v r}e\IeC {\v s}en\IeC {\'\i } koliz\IeC {\'\i }}{139}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}QoS}{141}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}HTML}{141}{subsection.5.1.7}
\contentsline {subsection}{\numberline {5.1.8}CSS}{142}{subsection.5.1.8}
\contentsline {section}{\numberline {5.2}S\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} slu\IeC {\v z}by pro webov\IeC {\'e} technologie}{144}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Rodina protokol\IeC {\r u} TCP/IP (ARP, IPv4, IPv6, ICMP, UDP, TCP)}{144}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Adresace, p\IeC {\v r}id\IeC {\v e}lov\IeC {\'a}n\IeC {\'\i } adres, p\IeC {\v r}evod mezi IP adresami a adresami linkov\IeC {\'e} vrstvy, routing, fragmentace, spolehlivost, flow control, congestion control, NAT}{144}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Rozhran\IeC {\'\i } BSD sockets}{145}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Spolehlivost - spojovan\IeC {\'e} a nespojovan\IeC {\'e} protokoly, typy, detekce a oprava chyb}{145}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Bezpe\IeC {\v c}nost - IPSec, SSL, firewalls}{145}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Internetov\IeC {\'e} a intranetov\IeC {\'e} protokoly a technologie - DNS, SMTP, IMAP, POP3, FTP, HTTP, NFS}{145}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}IP telefonie. Desetimegabitov\IeC {\'e}, stomegabitov\IeC {\'e}, gigabitov\IeC {\'e} verze ethernetu, \IeC {\v r}\IeC {\'\i }zen\IeC {\'\i } toku. Bezdr\IeC {\'a}tov\IeC {\'y} ethernet (protokolov\IeC {\'a} architektura 802.11, p\IeC {\v r}\IeC {\'\i }stupov\IeC {\'e} metody, roaming). xDSL. Mobiln\IeC {\'\i } s\IeC {\'\i }t\IeC {\v e} (GSM, 3G). Bluetooth.}{145}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}HTTP}{145}{subsection.5.2.8}
\contentsline {section}{\numberline {5.3}Webov\IeC {\'e} slu\IeC {\v z}by}{146}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}SOA - Service oriented architecture}{146}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}XML}{147}{subsection.5.3.2}
\contentsline {subsubsection}{XPath}{147}{section*.44}
\contentsline {subsubsection}{XML Schema}{147}{section*.45}
\contentsline {subsubsection}{XSLT}{147}{section*.46}
\contentsline {subsection}{\numberline {5.3.3}Webov\IeC {\'a} slu\IeC {\v z}ba (Web service)}{148}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}SOAP}{148}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}WSDL}{150}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}WS-*}{150}{subsection.5.3.6}
\contentsline {subsubsection}{WS-Addressing}{150}{section*.47}
\contentsline {subsubsection}{WS-Security}{150}{section*.48}
\contentsline {subsubsection}{WS-Policy}{151}{section*.49}
\contentsline {subsection}{\numberline {5.3.7}UDDI}{151}{subsection.5.3.7}
\contentsline {subsection}{\numberline {5.3.8}WS-BPEL}{152}{subsection.5.3.8}
\contentsline {subsection}{\numberline {5.3.9}REST}{153}{subsection.5.3.9}
\contentsline {subsection}{\numberline {5.3.10}S\IeC {\'e}mantick\IeC {\'e} webov\IeC {\'e} slu\IeC {\v z}by}{154}{subsection.5.3.10}
\contentsline {section}{\numberline {5.4}Architektura klient-server aplikac\IeC {\'\i }}{155}{section.5.4}
\contentsline {section}{\numberline {5.5}Skriptov\IeC {\'a}n\IeC {\'\i } na stran\IeC {\v e} serveru}{157}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}PHP}{157}{subsection.5.5.1}
\contentsline {section}{\numberline {5.6}Skriptov\IeC {\'a}n\IeC {\'\i } na stran\IeC {\v e} klienta}{161}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}JavaScript}{162}{subsection.5.6.1}
\contentsline {section}{\numberline {5.7}Webov\IeC {\'e} frameworky}{165}{section.5.7}
\contentsline {section}{\numberline {5.8}Pou\IeC {\v z}it\IeC {\'\i } datab\IeC {\'a}zov\IeC {\'y}ch syst\IeC {\'e}m\IeC {\r u} ve webov\IeC {\'y}ch aplikac\IeC {\'\i }ch, NoSQL datab\IeC {\'a}ze, multimedi\IeC {\'a}ln\IeC {\'\i } datab\IeC {\'a}ze}{167}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}NOSQL datab\IeC {\'a}ze}{167}{subsection.5.8.1}
\contentsline {subsubsection}{HDFS}{169}{section*.50}
\contentsline {subsubsection}{MapReduce}{169}{section*.51}
\contentsline {subsubsection}{Key-value}{170}{section*.52}
\contentsline {subsubsection}{Column-family stores}{171}{section*.53}
\contentsline {subsubsection}{Dokumentov\IeC {\'e} datab\IeC {\'a}ze}{171}{section*.54}
\contentsline {subsubsection}{Grafov\IeC {\'e} datab\IeC {\'a}ze}{172}{section*.55}
\contentsline {subsubsection}{Multi-model datab\IeC {\'a}ze}{172}{section*.56}
\contentsline {subsubsection}{Mics}{172}{section*.57}
\contentsline {section}{\numberline {5.9}Indexace a prohled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } dokument\IeC {\r u}, principy fungov\IeC {\'a}n\IeC {\'\i } webov\IeC {\'y}ch vyhled\IeC {\'a}va\IeC {\v c}\IeC {\r u}}{173}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Vyhled\IeC {\'a}va\IeC {\v c}e}{173}{subsection.5.9.1}
\contentsline {subsection}{\numberline {5.9.2}Page ranking}{174}{subsection.5.9.2}
\contentsline {subsection}{\numberline {5.9.3}Search engine ranking and SEO}{175}{subsection.5.9.3}
\contentsline {subsection}{\numberline {5.9.4}Recommender systems}{176}{subsection.5.9.4}
\contentsline {subsection}{\numberline {5.9.5}Booleovsk\IeC {\'y} model}{176}{subsection.5.9.5}
\contentsline {subsection}{\numberline {5.9.6}Vektorov\IeC {\'y} model}{177}{subsection.5.9.6}
\contentsline {subsection}{\numberline {5.9.7}Vyhled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } v multimedi\IeC {\'\i }ch}{177}{subsection.5.9.7}
\contentsline {subsection}{\numberline {5.9.8}Podobnost}{178}{subsection.5.9.8}
\contentsline {subsection}{\numberline {5.9.9}Podobnostn\IeC {\'\i } funkce}{178}{subsection.5.9.9}
\contentsline {subsection}{\numberline {5.9.10}Glob\IeC {\'a}ln\IeC {\'\i } deskriptory obr\IeC {\'a}zk\IeC {\r u}}{179}{subsection.5.9.10}
\contentsline {subsection}{\numberline {5.9.11}Lok\IeC {\'a}ln\IeC {\'\i } deskriptory obr\IeC {\'a}zk\IeC {\r u}}{180}{subsection.5.9.11}
\contentsline {subsection}{\numberline {5.9.12}Neuronov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{181}{subsection.5.9.12}
\contentsline {subsection}{\numberline {5.9.13}2D a 3D modely}{181}{subsection.5.9.13}
\contentsline {subsection}{\numberline {5.9.14}Video, Audio}{182}{subsection.5.9.14}
\contentsline {subsection}{\numberline {5.9.15}Metrick\IeC {\'e} indexov\IeC {\'a}n\IeC {\'\i } podobnosti - TODO}{183}{subsection.5.9.15}
\contentsline {section}{\numberline {5.10}Linked Data}{184}{section.5.10}
\contentsline {subsection}{\numberline {5.10.1}Principy}{184}{subsection.5.10.1}
\contentsline {subsection}{\numberline {5.10.2}Integrace s\IeC {\'e}mantick\IeC {\'y}ch dat do webov\IeC {\'y}ch str\IeC {\'a}nek}{184}{subsection.5.10.2}
\contentsline {subsection}{\numberline {5.10.3}RDF}{185}{subsection.5.10.3}
\contentsline {subsection}{\numberline {5.10.4}RDF Schema}{186}{subsection.5.10.4}
\contentsline {subsection}{\numberline {5.10.5}SPARQL}{187}{subsection.5.10.5}
\contentsline {subsection}{\numberline {5.10.6}Linked data platform}{187}{subsection.5.10.6}
\contentsline {subsection}{\numberline {5.10.7}Social web protocols}{187}{subsection.5.10.7}
\contentsline {section}{\numberline {5.11}Zaji\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } bezpe\IeC {\v c}nosti informa\IeC {\v c}n\IeC {\'\i }ch syst\IeC {\'e}m\IeC {\r u} v prost\IeC {\v r}ed\IeC {\'\i } internetu, autentizace, autorizace, bezpe\IeC {\v c}nostn\IeC {\'\i } modely, z\IeC {\'a}klady \IeC {\v s}ifrov\IeC {\'a}n\IeC {\'\i }, ochrana dat}{188}{section.5.11}
\contentsline {subsection}{\numberline {5.11.1}Modely}{189}{subsection.5.11.1}
\contentsline {subsection}{\numberline {5.11.2}Autorizace}{190}{subsection.5.11.2}
\contentsline {subsection}{\numberline {5.11.3}Autentizace}{191}{subsection.5.11.3}
\contentsline {subsection}{\numberline {5.11.4}\IeC {\v S}ifrov\IeC {\'a}n\IeC {\'\i }}{191}{subsection.5.11.4}
\contentsline {subsection}{\numberline {5.11.5}Prost\IeC {\v r}ed\IeC {\'\i } internetu}{192}{subsection.5.11.5}
\contentsline {subsection}{\numberline {5.11.6}Bezpe\IeC {\v c}nost v datab\IeC {\'a}z\IeC {\'\i }ch}{193}{subsection.5.11.6}
