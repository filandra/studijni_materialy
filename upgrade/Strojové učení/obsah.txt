Snaha je nahlížet probírané modely jako používané algoritmy i z pohledu statistiků (viz první kniha Literatury).

Lineární regrese a učení založené na instancích jakožto extrémy v prostoru možných modelů,

prokletí dimenzionality, bias-variance tradeoff,

logistická regrese, zobecněné lineární modely,

ohodnocování modelů (odhad chyby a interval spolehlivosti, krosvalidace, one-leave-out),

rozhodovací stromy, prořezávání, chybějící hodnoty,

tvorba pravidel PRIM,

kombinace modelů (AdaBoost, random forest),

support vector machines,

Bayesovské učení, EM algoritmus na příkladu klastrování,

latent dirichlet allocation, gaussovské procesy a bayesovská optimalizace,

učení bez učitele - nákupní košík (Apriori alg.), klastrování k-means, k-medoids, hierarchické, 
